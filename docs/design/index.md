# REQ-purpose
The goal of Time Puncher is to be a simple and easy to use time clock application.

The user should be able to easily track the time spent on some tasks for different time frames. Once some time has been tracked, they should be able to review the time spent with different statistics provided by the application. Moreover, the user should be able to categorize their task with some tags, and pull statistics for a subset of tasks.

This document is a work in progress and for the moment, describes only a minimum viable product (MVP). The following specifications describe the MVP:
- [[SPC-gui]]: Interface of the application;
- [[SPC-data]]: Data persisted by the application;
- [[SPC-config]]: Application's configuration.