# SPC-config
partof: REQ-purpose
###

The user shall be able to configure the application with either command line options or a configuration file. The configuration file will be written in Toml format.

## [[.file]]
The application will look for a configuration file in Toml format in the following places:
1. `--config <path>` (or `-c <path>`) when launching the application via the command line;
2. `<CONFIG_HOME>/timepuncher/config.toml` where `<CONFIG_HOME>` is OS specific:
    - `$XDG_CONFIG_HOME` on Linux;
    - `%APPDATA%` on Windows.

If no configuration is found, then the default value are used.

The available configuration keys are detailed below. When looking for a key, the application first look at the command line, then in the configuration file, and finally uses its default value.

## [[.data]]: JSON Data File
- Key: `data`
- Type: path
- Long option: `--data <path>`
- Short option: `-d <path>`
- Default value: `<CONFIG_HOME>/timepuncher/data.json`
- Description: Path to the JSON file where the application data is stored.

> Note: A path is either absolute or relative to the working directory for a command line option, and to the configuration file for an entry in it.