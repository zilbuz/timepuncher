# SPC-gui
partof: REQ-purpose
###

The minimum viable product shall be a graphical utility that can only record time without specifying any task. The output of the graphical utility is the current time recorded, the total time recorded for the day and the total time recorded for the week.

The utility contains a single window with the following:
- Start button
- Pause button
- Reset button
- Field with the current time recorded
- Field with the total time recorded for the day
- Field with the total time recorded for the week

There is three states to the applications:
- **Stopped** (*startup state*): The chronometer is stopped and has a value of 0;
- **Started**: The chronometer is started;
- **Paused**: The chronometer is stopped and has a value different of 0.

Each button can be active or inactive, and the user can click on an active button to transition the application from one state to another. The following diagram describe these transitions with the state of the buttons.

```plaintext
                  +---------------------+
                  |       Stopped       |
                  |                     |
                  | Start btn: active   |
                  | Pause btn: inactive |
                  | Reset btn: inactive |
                  +------+---------+----+
                         |         ^
                         |         |
          Clicked Start  |         |
          +--------------+         |
          |                        |
          |                        |
          v                        |
+---------+-----------+            |
|       Started       |            |
|                     |            |
| Start btn: inactive |            |Clicked Reset
| Pause btn: active   |            |
| Reset btn: inactive |            |
+----+----------+-----+            |
     ^          |                  |
     |          | Clicked Pause    |
     |          +------------+     |
     |                       |     |
     | Clicked Start         |     |
     +-----------------+     |     |
                       |     v     |
                  +----+-----+-----+----+
                  |       Paused        |
                  |                     |
                  | Start btn: active   |
                  | Pause btn: inactive |
                  | Reset btn: active   |
                  +---------------------+
```
