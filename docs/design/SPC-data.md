# SPC-data
partof: REQ-purpose
###

The data for the application is stored in a JSON file with the following fields:
- `last_update`
- `elapsed`
  - `day`
  - `week`

The `elapsed` values are stored as a number of seconds. The `last_update` field is an ISO8601 date stored in a string.

The `last_update` date is read when launching the application and compared to the current date to know if the elapsed times for the day and week should be reset. The current date is then written as soon as the application starts recording some time.

The elapsed fields are continuously updated while the application records the elapsed time.