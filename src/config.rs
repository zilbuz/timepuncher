use std::{
    env,
    error::Error,
    fs,
    path::{Path, PathBuf},
};

use crate::cli::Args;

/// Represent the configuration of the application
// #SPC-config
#[derive(Debug, PartialEq, Clone)]
pub struct Config {
    /// Path to the configuration file
    // #SPC-config.file
    config_file: PathBuf,
    /// Path to the data file
    // #SPC-config.data
    data_file: PathBuf,
}

impl Config {
    /// Find the configuration folder when on windows
    #[cfg(windows)]
    fn find_config_folder() -> Result<PathBuf, Box<Error>> {
        Ok(env::var("APPDATA")?.into())
    }

    /// Find the configuration folder when on unix
    #[cfg(unix)]
    fn find_config_folder() -> Result<PathBuf, Box<Error>> {
        use env::VarError;

        let config_home = env::var("XDG_CONFIG_HOME");
        let config_home = if let Err(VarError::NotPresent) = config_home {
            let mut config_home: PathBuf = env::var("HOME")?.into();
            config_home.push(".config");
            config_home
        } else {
            config_home?.into()
        };
        Ok(config_home)
    }

    /// Build a default Config struct
    fn default() -> Result<Config, Box<Error>> {
        let mut config_folder = Config::find_config_folder()?;
        config_folder.push("timepuncher");

        let mut config = Config {
            config_file: config_folder.clone(),
            data_file: config_folder.clone(),
        };

        config.config_file.push("config.toml");
        config.data_file.push("data.json");

        Ok(config)
    }

    /// Build the configuration of the application according to the arguments given to the program.
    pub fn new(args: &Args) -> Result<Config, Box<Error>> {
        // Init default config
        let mut config = Config::default()?;

        // Look for a config file provided by the user
        if let Some(config_file) = &args.config {
            config.config_file = to_absolute(env::current_dir()?, config_file);
        }

        // Load configuration from file if it exists
        if config.config_file.exists() {
            serde::load(&mut config)?;
        }

        // Look for a data file provided by the user
        if let Some(data_file) = &args.data {
            config.data_file = to_absolute(env::current_dir()?, data_file);
        }

        // Create the parent folder of the data file if it doesn't exist
        if let Some(folder) = config.data_file.parent() {
            fs::create_dir_all(folder)?;
        }

        Ok(config)
    }

    /// Returns the path to the data file.
    pub fn data_file(&self) -> &Path {
        self.data_file.as_path()
    }

    /// Create a test configuration with files in the given directory.
    ///
    /// This function is only available for the tests.
    #[cfg(test)]
    pub fn new_in<P: AsRef<Path>>(dir: P) -> Config {
        Config {
            config_file: dir.as_ref().join("config.toml"),
            data_file: dir.as_ref().join("data.json"),
        }
    }
}

/// Convert a possibly relative path to an absolute path by joining it to the base.
///
/// If the given path is already absolute, then nothing is done.
///
/// The function doesn't check that the base path is absolute, so the resulting
/// path can still be relative if the base path is relative.
fn to_absolute<P: AsRef<Path>>(base: PathBuf, path: P) -> PathBuf {
    if path.as_ref().is_relative() {
        let mut absolute_path = base.clone();
        absolute_path.push(path);
        absolute_path
    } else {
        path.as_ref().into()
    }
}

mod serde {
    use super::to_absolute;
    use serde::{Deserialize, Serialize};
    use std::{error::Error, fs::File, io::Read, path::PathBuf};

    /// Serialized configuration
    #[derive(Debug, Serialize, Deserialize)]
    struct Config {
        /// Path to the data file
        data: Option<PathBuf>,
    }

    /// Load the configuration from disk.
    ///
    /// The file to load is taken from `config.config_file`, then the
    /// configuration is loaded in place.
    pub fn load(config: &mut super::Config) -> Result<(), Box<Error>> {
        let mut file = File::open(&config.config_file)?;
        let parent_folder: PathBuf = config.config_file.parent().unwrap().into();

        let mut content = String::new();
        file.read_to_string(&mut content)?;

        let serialized_config: Config = toml::from_str(&content)?;
        if let Some(data) = serialized_config.data {
            config.data_file = to_absolute(parent_folder, data);
        }

        Ok(())
    }
}

#[cfg(test)]
mod tests {
    use super::Config;
    use std::sync::Mutex;

    lazy_static::lazy_static! {
        /// Since the environment variables are global and the tests are executed in parallel, a
        /// race condition is possible between the different tests that use the same variables. This
        /// Mutex is here to guard the use of the environment in tests and **must** be manually
        /// locked before interacting with std::env.
        static ref ENV_GUARD: Mutex<()> = Mutex::new(());
    }

    #[test]
    fn to_absolute() {
        use super::to_absolute;

        use std::path::PathBuf;

        let base_path = PathBuf::from("/my/base/path");
        let absolute = PathBuf::from("/my/absolute/path");
        let relative = PathBuf::from("./my/relative/path");

        let expected_path = absolute.clone();
        assert_eq!(to_absolute(base_path.clone(), &absolute), expected_path);
        let expected_path = {
            let mut p = base_path.clone();
            p.push(relative.clone());
            p
        };
        assert_eq!(to_absolute(base_path.clone(), &relative), expected_path)
    }

    #[cfg(unix)]
    #[test]
    fn config_default_unix() {
        use std::{env, path::PathBuf};

        let home = PathBuf::from("/my/home");
        let xdg_home = PathBuf::from("/my/xdg/home");

        let expected_home_config = Config {
            config_file: home.join(".config/timepuncher/config.toml"),
            data_file: home.join(".config/timepuncher/data.json"),
        };
        let expected_xdg_home_config = Config {
            config_file: xdg_home.join("timepuncher/config.toml"),
            data_file: xdg_home.join("timepuncher/data.json"),
        };

        let _guard = ENV_GUARD.lock().unwrap();

        env::set_var("HOME", home);
        env::set_var("XDG_CONFIG_HOME", xdg_home);
        assert_eq!(Config::default().unwrap(), expected_xdg_home_config);

        env::remove_var("XDG_CONFIG_HOME");
        assert_eq!(Config::default().unwrap(), expected_home_config);

        env::remove_var("HOME");
        assert!(Config::default().is_err());
    }

    #[cfg(windows)]
    #[test]
    fn config_default_windows() {
        use std::{env, path::PathBuf};

        let appdata = PathBuf::from(r#"c:\my\appdata"#);
        let expected_appdata_config = Config {
            config_file: appdata.join("timepuncher/config.toml"),
            data_file: appdata.join("timepuncher/data.json"),
        };

        let _guard = ENV_GUARD.lock().unwrap();

        env::set_var("APPDATA", appdata);
        assert_eq!(Config::default().unwrap(), expected_appdata_config);

        env::remove_var("APPDATA");
        assert!(Config::default().is_err());
    }

    #[test]
    fn config_new() {
        use super::Args;
        use std::{env, fs::File, io::Write};
        use tempfile::tempdir;

        // Create temp dir to store config and data files
        let tmp_dir1 = tempdir().expect("Unable to create temp dir");
        let tmp_dir2 = tempdir().expect("Unable to create secondary temp dir");
        let tmp_dir3 = tempdir().expect("Unable to create third temp dir");

        // Setup config and data file paths that will be tested
        let config_without_data_file_path = tmp_dir1.path().join("config_without_data.toml");
        let config_with_rel_data_file_path = tmp_dir1.path().join("config_with_rel_data.toml");
        let config_with_abs_data_file_path = tmp_dir1.path().join("config_with_abs_data.toml");
        let data_from_config_rel_file_path = tmp_dir1.path().join("data_rel_from_config.json");
        let data_from_config_abs_file_path = tmp_dir2.path().join("data_abs_from_config.json");

        // Setup config folder to point to a new temporary directory so that no existing config files will conflict with
        // the tested files
        let _guard = ENV_GUARD.lock().unwrap();
        if cfg!(unix) {
            env::set_var("XDG_CONFIG_HOME", tmp_dir3.path());
        } else if cfg!(windows) {
            env::set_var("APPDATA", tmp_dir3.path());
        } else {
            panic!("Unsupported test platform");
        }

        // Create a configuration file without a path to the data file
        File::create(config_without_data_file_path.clone()).expect("Unable to create config file");

        // Create a configuration file with a relative path to the data file
        {
            let mut config_file = File::create(config_with_rel_data_file_path.clone())
                .expect("Unable to create config file");
            writeln!(
                config_file,
                r#"data = '{}'"#,
                data_from_config_rel_file_path
                    .file_name()
                    .expect("No file name")
                    .to_str()
                    .expect("Unable to convert file name to UTF-8")
            )
            .expect("Unable to write to config file");
        }
        // Create a configuration file with an absolute path to the data file
        {
            let mut config_file = File::create(config_with_abs_data_file_path.clone())
                .expect("Unable to create config file");
            writeln!(
                config_file,
                r#"data = '{}'"#,
                data_from_config_abs_file_path
                    .to_str()
                    .expect("Unable to convert file path to UTF-8")
            )
            .expect("Unable to write to config file");
        }

        // Create a default configuration
        let default = Config::default().expect("Unable to create default configuration");

        // Test that a new configuration without arguments is equal to a default configuration
        {
            let empty_args = Args {
                config: None,
                data: None,
            };
            assert_eq!(Config::new(&empty_args).unwrap(), default);
        }

        // Test a configuration with a configuration file passed as argument
        {
            // Without data file path
            let args_with_config = Args {
                config: Some(config_without_data_file_path.clone()),
                data: None,
            };
            assert_eq!(
                Config::new(&args_with_config).unwrap(),
                Config {
                    config_file: config_without_data_file_path.clone(),
                    data_file: default.data_file.clone()
                }
            );

            // With a relative data file path
            let args_with_config = Args {
                config: Some(config_with_rel_data_file_path.clone()),
                data: None,
            };
            assert_eq!(
                Config::new(&args_with_config).unwrap(),
                Config {
                    config_file: config_with_rel_data_file_path.clone(),
                    data_file: data_from_config_rel_file_path.clone()
                }
            );

            // With an absolute data file path
            let args_with_config = Args {
                config: Some(config_with_abs_data_file_path.clone()),
                data: None,
            };
            assert_eq!(
                Config::new(&args_with_config).unwrap(),
                Config {
                    config_file: config_with_abs_data_file_path.clone(),
                    data_file: data_from_config_abs_file_path.clone()
                }
            );
        }

        // Test a configuration with both the configuration file and the data file passed as arguments
        {
            let args_with_config_and_data = Args {
                config: Some(config_with_rel_data_file_path.clone()),
                data: Some(data_from_config_abs_file_path.clone()),
            };
            assert_eq!(
                Config::new(&args_with_config_and_data).unwrap(),
                Config {
                    config_file: config_with_rel_data_file_path.clone(),
                    data_file: data_from_config_abs_file_path.clone(),
                }
            );
        }
    }
}
