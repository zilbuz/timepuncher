use relm::Widget;
use std::error::Error;

mod cli;
mod config;
mod data;
mod ui;

fn main() -> Result<(), Box<Error>> {
    // Retrieve the arguments of the application
    let args = cli::args();
    // Retrieve the configuration from the arguments
    let config = config::Config::new(&args)?;
    // Try to load an existing data file, use Elapsed::zero() if it doesn't exist.
    let elapsed =
        data::Elapsed::load_from(config.data_file()).unwrap_or_else(|_| data::Elapsed::zero());
    // Launch UI
    ui::MainWindow::run((config, elapsed)).unwrap();
    Ok(())
}
