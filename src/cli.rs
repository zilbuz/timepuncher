use std::{
    ffi::{OsStr, OsString},
    path::{Path, PathBuf},
};
use structopt::StructOpt;

/// Check if the path given as parameter exists, and return it as a PathBuf if
/// that's the case.
fn file_exists(path: &OsStr) -> Result<PathBuf, OsString> {
    let path = Path::new(path);
    if path.exists() {
        Ok(path.into())
    } else {
        Err(OsString::from("File doesn't exist"))
    }
}

/// Structure representing the arguments of the programs.
#[derive(Debug, StructOpt)]
#[structopt(name = "Time Puncher")]
pub struct Args {
    /// Sets a custom config file
    #[structopt(short, long, value_name = "FILE")]
    #[structopt(parse(try_from_os_str = "file_exists"))]
    pub config: Option<PathBuf>,

    /// Sets a custom data file
    #[structopt(short, long, value_name = "FILE")]
    #[structopt(parse(from_os_str))]
    pub data: Option<PathBuf>,
}

/// Retrieve the arguments given to the program and return the Args struct.
pub fn args() -> Args {
    Args::from_args()
}

#[cfg(test)]
mod tests {
    #[test]
    fn file_exists() {
        use super::file_exists;

        use std::ffi::OsString;
        use tempfile::NamedTempFile;

        let existing_file = NamedTempFile::new().unwrap();

        let unexisting_file_path = OsString::from("/file/that/doesnt/exist");
        let existing_file_path = existing_file.path().as_os_str();

        assert!(file_exists(&unexisting_file_path).is_err());
        assert!(file_exists(existing_file_path).is_ok());
    }
}
