use chrono::{DateTime, Duration, Utc};
use gtk::prelude::*;
use relm::{connect, connect_stream, timeout, Relm, Update, Widget, WidgetTest};
use relm_derive::Msg;

use crate::{config::Config, data::Elapsed};

/// Messages that can be passed to the main window.
///
/// - Start, Pause, Reset: update the model state machine;
/// - Tick: update the labels with the current timings;
/// - Quit: Exit the application
#[derive(Msg, Debug, PartialEq)]
pub enum Msg {
    Start,
    Pause,
    Reset,
    Tick,
    Quit,
}

/// State of the main window
#[derive(Debug, PartialEq, Clone)]
enum State {
    /// The chronometer is stopped
    Stopped,
    /// The chronometer is started
    Started,
    /// The chronometer is paused
    Paused,
}

/// Model of the main window
#[derive(Clone)]
pub struct Model {
    /// Event stream  handler
    relm: Relm<MainWindow>,
    /// Application configuration
    config: Config,
    /// Current state of the window
    state: State,
    /// Time when the chronometer was started
    start_time: DateTime<Utc>,
    /// Elapsed time when the application was last paused.
    elapsed_on_last_pause: Elapsed,
}

impl Model {
    /// Create a model from the given configuration and existing elapsed times.
    fn new(relm: &Relm<MainWindow>, config: Config, elapsed: Elapsed) -> Self {
        Model {
            relm: relm.clone(),
            config,
            state: State::Stopped,
            start_time: Utc::now(),
            elapsed_on_last_pause: elapsed,
        }
    }

    /// Update the state machine of the model.
    ///
    /// The current state and given message are inspected to know which action to take. In case of an invalid
    /// transition, an error is logged to the error output and nothing else is done.
    pub fn update(&mut self, msg: Msg) {
        match (&self.state, msg) {
            (State::Stopped, Msg::Start) => self.start(),
            (State::Started, Msg::Pause) => self.pause(),
            (State::Paused, Msg::Start) => self.start(),
            (State::Paused, Msg::Reset) => self.reset(),
            (state, msg) => eprintln!(
                "Invalid transition from state {:?} with message {:?}",
                state, msg
            ),
        }
    }

    /// Retrieve the currently elapsed time.
    pub fn elapsed(&self) -> Elapsed {
        if self.state == State::Started {
            self.elapsed_on_last_pause.clone() + (Utc::now() - self.start_time)
        } else {
            self.elapsed_on_last_pause.clone()
        }
    }

    /// Set the model state machine to the "Started" state.
    fn start(&mut self) {
        self.start_time = Utc::now();
        self.state = State::Started;
    }
    /// Set the model state machine to the "Paused" state.
    fn pause(&mut self) {
        self.elapsed_on_last_pause = self.elapsed();
        self.state = State::Paused;
    }
    /// Set the model state machine to the "Stopped" state.
    fn reset(&mut self) {
        self.elapsed_on_last_pause.session = Duration::zero();
        self.state = State::Stopped;
    }
}

/// Main window of the application
// #SPC-gui
#[derive(Clone)]
pub struct MainWindow {
    /// The model used by the main window
    model: Model,
    /// The GTK top-level window
    window: gtk::Window,
    /// The GTK start button
    start: gtk::Button,
    /// The GTK pause button
    pause: gtk::Button,
    /// The GTK reset button
    reset: gtk::Button,
    /// The GTK elapsed label for the current session
    elapsed: gtk::Label,
    /// The GTK elapsed label for the current day
    elapsed_day: gtk::Label,
    /// The GTK elapsed label for the current week
    elapsed_week: gtk::Label,
}

/// Format the duration to a string
fn duration_to_string(mut duration: Duration) -> String {
    let hours = duration.num_hours();
    duration = duration - Duration::hours(hours);
    let minutes = duration.num_minutes();
    duration = duration - Duration::minutes(minutes);
    let seconds = duration.num_seconds();
    format!("{:02}:{:02}:{:02}", hours, minutes, seconds)
}

impl MainWindow {
    /// Update the window's buttons sensitivity according to the model's state
    fn update_buttons_sensitivity(&self) {
        self.start.set_sensitive(self.model.state != State::Started);
        self.pause.set_sensitive(self.model.state == State::Started);
        self.reset.set_sensitive(self.model.state == State::Paused);
    }

    /// Update the window's labels according to the given timings
    fn update_labels(&self, elapsed: &Elapsed) {
        self.elapsed.set_text(&duration_to_string(elapsed.session));
        self.elapsed_day.set_text(&duration_to_string(elapsed.day));
        self.elapsed_week
            .set_text(&duration_to_string(elapsed.week));
    }

    /// Update the state of the application according to the elapsed times.
    fn tick(&self) {
        let elapsed = self.model.elapsed();
        self.update_labels(&elapsed);
        if self.model.state == State::Started {
            // Trigger a new Tick message in 1 second if the chronometer is started.
            timeout(self.model.relm.stream(), 1000, || Msg::Tick);
        }
        // Save the current timings to the data file so that the data isn't lost on crash.
        elapsed.save_to(self.model.config.data_file()).unwrap();
    }
}

impl Widget for MainWindow {
    type Root = gtk::Window;

    fn root(&self) -> Self::Root {
        self.window.clone()
    }

    fn view(relm: &Relm<Self>, model: Self::Model) -> Self {
        let builder = gtk::Builder::new_from_string(include_str!("main_window.glade"));

        macro_rules! get {
            ($id:literal as $type:ty) => {
                builder.get_object::<$type>($id).unwrap()
            };
        }

        let main_window = MainWindow {
            model,
            window: get!("main_window" as gtk::Window),
            start: get!("start" as gtk::Button),
            pause: get!("pause" as gtk::Button),
            reset: get!("reset" as gtk::Button),
            elapsed: get!("elapsed" as gtk::Label),
            elapsed_day: get!("elapsed_day" as gtk::Label),
            elapsed_week: get!("elapsed_week" as gtk::Label),
        };

        // Setup window class
        #[allow(deprecated)]
        main_window
            .window
            .set_wmclass("Time Puncher", "Time Puncher");

        // Update labels with the data in the model
        main_window.update_labels(&main_window.model.elapsed());

        connect!(
            relm,
            main_window.window,
            connect_delete_event(_, _),
            return (Some(Msg::Quit), Inhibit(false))
        );
        connect!(relm, main_window.start, connect_clicked(_), Msg::Start);
        connect!(relm, main_window.pause, connect_clicked(_), Msg::Pause);
        connect!(relm, main_window.reset, connect_clicked(_), Msg::Reset);

        main_window.window.show_all();

        main_window
    }
}

impl WidgetTest for MainWindow {
    type Widgets = MainWindow;

    fn get_widgets(&self) -> Self::Widgets {
        self.clone()
    }
}

impl Update for MainWindow {
    type Model = Model;
    type ModelParam = (Config, Elapsed);
    type Msg = Msg;

    fn model(relm: &Relm<Self>, params: Self::ModelParam) -> Self::Model {
        Model::new(relm, params.0, params.1)
    }

    fn update(&mut self, event: Self::Msg) {
        use Msg::*;

        match event {
            Start | Pause | Reset => {
                self.model.update(event);
                self.update_buttons_sensitivity();
                self.tick();
            }
            Tick => self.tick(),
            Quit => gtk::main_quit(),
        }
    }
}

#[cfg(test)]
mod tests {
    use super::MainWindow;
    use gtk::prelude::*;
    use gtk_test::{assert_text, click, wait};

    #[test]
    #[ignore]
    #[allow(clippy::cyclomatic_complexity)]
    fn chrono() {
        use crate::{config::Config, data::Elapsed};

        let tempdir = tempfile::tempdir().expect("Unable to retrieve a temporary directory");
        let config = Config::new_in(&tempdir);
        let elapsed = Elapsed::zero();
        let (_component, widgets) = relm::init_test::<MainWindow>((config, elapsed)).unwrap();
        widgets.window.activate_focus();

        // Check initial status
        assert!(widgets.start.is_sensitive());
        assert!(!widgets.pause.is_sensitive());
        assert!(!widgets.reset.is_sensitive());
        assert_text!(widgets.elapsed, "00:00:00");
        assert_text!(widgets.elapsed_day, "00:00:00");
        assert_text!(widgets.elapsed_week, "00:00:00");

        // Check that the chronometer starts when clicking on "start"
        click(&widgets.start);
        let elapsed_text = widgets.elapsed.get_text();
        let elapsed_day_text = widgets.elapsed_day.get_text();
        let elapsed_week_text = widgets.elapsed_week.get_text();
        wait(2000);
        assert!(!widgets.start.is_sensitive());
        assert!(widgets.pause.is_sensitive());
        assert!(!widgets.reset.is_sensitive());
        assert_ne!(elapsed_text, widgets.elapsed.get_text());
        assert_ne!(elapsed_day_text, widgets.elapsed_day.get_text());
        assert_ne!(elapsed_week_text, widgets.elapsed_week.get_text());

        // Check that the chronometer stops when clicking on "pause"
        click(&widgets.pause);
        let elapsed_text = widgets.elapsed.get_text();
        let elapsed_day_text = widgets.elapsed_day.get_text();
        let elapsed_week_text = widgets.elapsed_week.get_text();
        wait(2000);
        assert!(widgets.start.is_sensitive());
        assert!(!widgets.pause.is_sensitive());
        assert!(widgets.reset.is_sensitive());
        assert_eq!(elapsed_text, widgets.elapsed.get_text());
        assert_eq!(elapsed_day_text, widgets.elapsed_day.get_text());
        assert_eq!(elapsed_week_text, widgets.elapsed_week.get_text());

        // Check that the chronometer starts again when clicking on "start"
        click(&widgets.start);
        let elapsed_text = widgets.elapsed.get_text();
        let elapsed_day_text = widgets.elapsed_day.get_text();
        let elapsed_week_text = widgets.elapsed_week.get_text();
        wait(2000);
        assert!(!widgets.start.is_sensitive());
        assert!(widgets.pause.is_sensitive());
        assert!(!widgets.reset.is_sensitive());
        assert_ne!(elapsed_text, widgets.elapsed.get_text());
        assert_ne!(elapsed_day_text, widgets.elapsed_day.get_text());
        assert_ne!(elapsed_week_text, widgets.elapsed_week.get_text());

        // Check that the times are reset and the chronometer is stopped when clicking on "pause" then "reset".
        click(&widgets.pause);
        click(&widgets.reset);
        assert_text!(widgets.elapsed, "00:00:00");
        let elapsed_day_text = widgets.elapsed_day.get_text();
        let elapsed_week_text = widgets.elapsed_week.get_text();
        wait(2000);
        assert!(widgets.start.is_sensitive());
        assert!(!widgets.pause.is_sensitive());
        assert!(!widgets.reset.is_sensitive());
        assert_text!(widgets.elapsed, "00:00:00");
        assert_eq!(elapsed_day_text, widgets.elapsed_day.get_text());
        assert_eq!(elapsed_week_text, widgets.elapsed_week.get_text());
    }
}
