use chrono::{Duration, Local, Utc};
use std::{
    error::Error,
    fs::File,
    io::{BufReader, BufWriter},
    ops::{Add, AddAssign},
    path::Path,
};

/// Elapsed time recorded by the application.
#[derive(Debug, Clone, PartialEq)]
pub struct Elapsed {
    /// Elapsed time during the current session
    pub session: Duration,
    /// Elapsed time for the current day
    pub day: Duration,
    /// Elapsed time for the current week
    pub week: Duration,
}

impl Elapsed {
    /// Build Elapsed with zero elapsed time for all its fields
    pub fn zero() -> Elapsed {
        Elapsed {
            session: Duration::zero(),
            day: Duration::zero(),
            week: Duration::zero(),
        }
    }

    /// Load the elapsed times from a data file.
    ///
    /// If the current date is different from the last updated date of the data
    /// file, then `elapsed.day` is reset.
    ///
    /// If the current week is different from the last updated date of the data
    /// file, then `elapsed.week` is reset.
    pub fn load_from<P: AsRef<Path>>(path: P) -> Result<Elapsed, Box<Error>> {
        let file = File::open(path)?;
        let reader = BufReader::new(file);

        let data: serde::ApplicationData = serde_json::from_reader(reader)?;

        let mut elapsed = Elapsed::zero();

        // Retrieve start of current day
        let start_of_day = Local::today().and_hms(0, 0, 0);

        // Retrieve start of current week
        use chrono::Datelike;
        let start_of_week =
            start_of_day - Duration::days(i64::from(start_of_day.weekday().num_days_from_monday()));

        // Convert last update to the local timezone
        let last_update = data.last_update.with_timezone(&start_of_day.timezone());

        // If the last update is during the current day, use serialized elapsed daily time
        if last_update >= start_of_day {
            elapsed.day = Duration::seconds(data.elapsed.day);
        }

        // If the last update is during the current week, use serialized elapsed weekly time
        if last_update >= start_of_week {
            elapsed.week = Duration::seconds(data.elapsed.week);
        }

        Ok(elapsed)
    }

    /// Save the elapsed times to a data file.
    pub fn save_to<P: AsRef<Path>>(&self, path: P) -> Result<(), Box<Error>> {
        let data = serde::ApplicationData {
            last_update: Utc::now(),
            elapsed: serde::Elapsed {
                day: self.day.num_seconds(),
                week: self.week.num_seconds(),
            },
        };

        let file = File::create(path)?;
        let writer = BufWriter::new(file);

        serde_json::to_writer_pretty(writer, &data)?;

        Ok(())
    }
}

/// Implements the addition of a single duration to all the fields of elapsed.
impl AddAssign<Duration> for Elapsed {
    fn add_assign(&mut self, duration: Duration) {
        self.session = self.session + duration;
        self.day = self.day + duration;
        self.week = self.week + duration;
    }
}

/// Implements the addition of a single duration to all the fields of elapsed.
impl Add<Duration> for Elapsed {
    type Output = Elapsed;

    fn add(mut self, duration: Duration) -> Elapsed {
        self += duration;
        self
    }
}

mod serde {
    use chrono::{DateTime, Utc};
    use serde::{Deserialize, Serialize};

    /// Seralized elapsed times.
    #[derive(Debug, Serialize, Deserialize)]
    pub struct Elapsed {
        /// Number of elapsed seconds for the day.
        pub day: i64,
        /// Number of elapsed seconds for the week.
        pub week: i64,
    }

    /// Serialized data for the application.
    // #SPC-data
    #[derive(Debug, Serialize, Deserialize)]
    pub struct ApplicationData {
        /// Last updated time of the application data.
        pub last_update: DateTime<Utc>,
        /// Elapsed times.
        pub elapsed: Elapsed,
    }
}

#[cfg(test)]
mod tests {
    use super::Elapsed;

    #[test]
    fn save_to() {
        use chrono::{DateTime, Duration, Utc};
        use serde_json::Value;
        use tempfile::NamedTempFile;

        let expected_last_update = Utc::now();
        let expected_elapsed_day = 2;
        let expected_elapsed_week = 3;

        let elapsed = Elapsed {
            session: Duration::seconds(1),
            day: Duration::seconds(expected_elapsed_day),
            week: Duration::seconds(expected_elapsed_week),
        };

        // Save the data on disk and sync the filesystem so that the data can be read in the rest of the test
        let path = NamedTempFile::new().expect("Unable to create temp file");
        assert!(elapsed.save_to(path.as_ref()).is_ok());
        path.as_file().sync_data().expect("Unable to sync data");

        // Read the file to check that the data has been correctly saved to disk
        let content: Value = serde_json::from_reader(path).expect("Unable to read saved data");
        // - Check that last_update is within a second of the current time
        let last_update: DateTime<Utc> = content["last_update"].as_str().unwrap().parse().unwrap();
        let time_diff = Duration::seconds((expected_last_update - last_update).num_seconds().abs());
        assert!(time_diff < Duration::seconds(1));
        // - Check that the elapsed time for the session isn't saved
        assert!(content["elapsed"].get("session").is_none());
        // - Check that the elapsed time for the day and week are saved
        assert_eq!(
            expected_elapsed_day,
            content["elapsed"]["day"].as_i64().unwrap()
        );
        assert_eq!(
            expected_elapsed_week,
            content["elapsed"]["week"].as_i64().unwrap()
        );
    }

    #[test]
    fn load_from_well_formed() {
        use chrono::Utc;
        use serde_json::json;
        use tempfile::NamedTempFile;

        let data = NamedTempFile::new().expect("Unable to create temp file");
        let content = json!({
            "last_update": Utc::now(),
            "elapsed": {
                "day": 0,
                "week": 0,
            }
        });
        serde_json::to_writer_pretty(&data, &content).expect("Unable to save test data");

        assert!(Elapsed::load_from(data).is_ok());
    }

    #[test]
    fn load_from_malformed() {
        use chrono::Utc;
        use serde_json::json;
        use tempfile::NamedTempFile;

        let data = NamedTempFile::new().expect("Unable to create temp file");

        // Completely malformed data
        serde_json::to_writer_pretty(
            &data,
            &json!({
                "malformed": "data"
            }),
        )
        .expect("Unable to save test data");
        assert!(Elapsed::load_from(&data).is_err());

        // Wrong date format
        serde_json::to_writer_pretty(
            &data,
            &json!({
                "last_update": "not a date",
                "elapsed": {
                    "day": 0,
                    "week": 0,
                }
            }),
        )
        .expect("Unable to save test data");
        assert!(Elapsed::load_from(&data).is_err());

        // Wrong day format
        serde_json::to_writer_pretty(
            &data,
            &json!({
                "last_update": Utc::now(),
                "elapsed": {
                    "day": "not a number",
                    "week": 0,
                }
            }),
        )
        .expect("Unable to save test data");
        assert!(Elapsed::load_from(&data).is_err());

        // Wrong week format
        serde_json::to_writer_pretty(
            &data,
            &json!({
                "last_update": Utc::now(),
                "elapsed": {
                    "day": 0,
                    "week": "not a number",
                }
            }),
        )
        .expect("Unable to save test data");
        assert!(Elapsed::load_from(&data).is_err());
    }

    #[test]
    fn load_from_today() {
        use chrono::{Duration, Local};
        use serde_json::json;
        use tempfile::NamedTempFile;

        let data = NamedTempFile::new().expect("Unable to create temp file");
        let content = json!({
            "last_update": Local::now(),
            "elapsed": {
                "day": 1,
                "week": 2,
            }
        });
        serde_json::to_writer_pretty(&data, &content).expect("Unable to save test data");

        assert_eq!(
            Elapsed::load_from(data).unwrap(),
            Elapsed {
                session: Duration::seconds(0),
                day: Duration::seconds(1),
                week: Duration::seconds(2),
            }
        );
    }

    #[test]
    fn load_from_yesterday() {
        use chrono::{Duration, Local};
        use serde_json::json;
        use tempfile::NamedTempFile;

        let data = NamedTempFile::new().expect("Unable to create temp file");
        let content = json!({
            "last_update": (Local::now() - Duration::days(1)),
            "elapsed": {
                "day": 1,
                // The week is set to 0 so that the test doesn't fail on a monday
                // (where yesterday is also a different week)
                "week": 0,
            }
        });
        serde_json::to_writer_pretty(&data, &content).expect("Unable to save test data");

        let _data2 = vec![
            "toto3".to_string(),
            "titi3".to_string(),
            "tutu3".to_string(),
        ];

        assert_eq!(
            Elapsed::load_from(data).unwrap(),
            Elapsed {
                session: Duration::seconds(0),
                day: Duration::seconds(0),
                week: Duration::seconds(0),
            }
        );
    }

    #[test]
    fn load_from_last_week() {
        use chrono::{Duration, Local};
        use serde_json::json;
        use tempfile::NamedTempFile;

        let data = NamedTempFile::new().expect("Unable to create temp file");
        let content = json!({
            "last_update": (Local::now() - Duration::days(7)),
            "elapsed": {
                "day": 1,
                "week": 2,
            }
        });
        serde_json::to_writer_pretty(&data, &content).expect("Unable to save test data");

        assert_eq!(Elapsed::load_from(data).unwrap(), Elapsed::zero());
    }
}
