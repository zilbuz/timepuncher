#!/usr/bin/bash

# Saner programming env: these switches turn some bugs into errors
set -o errexit -o pipefail -o noclobber -o nounset

# Display help
show_help () {
    echo "Usage: $0 <COMMAND>"
    echo
    echo "    CI script to build and test the project. The environment variable Configuration is "
    echo "    read to choose wether to build in \"Debug\" or \"Release\" mode."
    echo
    echo "    Commands:"
    echo "        help:     Print this help"
    echo "        build:    Builds the project"
    echo "        test:     Launch regular tests"
    echo "        test_ui:  Launch UI tests"
}

# Default build options
options=""
# Add --release in release mode
if [[ -n "${Configuration-}" && "$Configuration" == "Release" ]]; then
    options="$options --release"
fi

# Check that a single argument has been provided
if [[ $# -ne 1 ]]; then
    echo "Error: A single argument is required."
    echo
    show_help
    exit 1
fi

# Process argument
case "$1" in
    help|-h|--help)
        show_help
        exit 0
        ;;
    build)
        set -x
        cargo build $options
        ;;
    test)
        set -x
        cargo test $options
        ;;
    test_ui)
        set -x
        cargo test $options -- --ignored --test-threads=1
        ;;
    *)
        echo "Error: Unrecognized argument \"$1\""
        echo
        show_help
        exit 1
        ;;
esac
